vcpkg_from_github(
    OUT_SOURCE_PATH SOURCE_PATH
    REPO wxWidgets/phoenix
    REF fb335e8cc92bc7719e5181385b56f599b993631f
    SHA512 6a952969442f95ba4b9cf0505f1f03c683324d4b5d05299dc814995fb7dd58c0cf9ce28c6d1409eb26ce41779a964e7fa1553d58969ad02b0573d962412c97ed
    PATCHES
      ignore-stackwalker-GetAssertStackTrace.patch
      ignore-osx-textctl.patch
      ignore-CreateSurface-glcanvas.patch
)

# We need a copy of the wxWidgets source code to build doxygen xml, the doxygen xml is used to generate the C++ code
# This should match the vcpkg port of wxWidgets
# THIS DOES NOT GET COMPILED
vcpkg_from_github(    
    OUT_SOURCE_PATH WX_SOURCE_PATH
    REPO wxWidgets/wxWidgets
    REF v3.2.5
    SHA512 6d92635f88fc7b0ee52ef996f87d79d9af34428e2cb4469134b74514135b87b42acbc515c0d2d1e1ff5265b8383c9783ce58655bd410fa9215cfc7a8d528aa87 
    HEAD_REF master
)

## Create __version__.py
set(VERSION_MAJOR 4)
set(VERSION_MINOR 2)
set(VERSION_RELEASE 0)
set(VERSION_STRING "${VERSION_MAJOR}.${VERSION_MINOR}.${VERSION_RELEASE}")
set(VERSION_BUILD_TYPE "")
configure_file( "${CMAKE_CURRENT_LIST_DIR}/cmake/__version__.py.in" "${SOURCE_PATH}/wx/__version__.py" @ONLY )

find_program(GIT NAMES git git.cmd)

file(COPY ${WX_SOURCE_PATH}/	DESTINATION ${SOURCE_PATH}/ext/wxWidgets/)
file(COPY ${CMAKE_CURRENT_LIST_DIR}/CMakeLists.txt DESTINATION ${SOURCE_PATH})
file(COPY ${CMAKE_CURRENT_LIST_DIR}/cmake/ DESTINATION ${SOURCE_PATH}/cmake/)

x_vcpkg_get_python_packages(PYTHON_VERSION "3" OUT_PYTHON_VAR "PYTHON3" REQUIREMENTS_FILE ${SOURCE_PATH}/requirements.txt )

# We need bash.exe for the wxPython preprocessor
if(CMAKE_HOST_WIN32)
    vcpkg_acquire_msys(MSYS_ROOT PACKAGES bash)
    vcpkg_add_to_path(PREPEND "${MSYS_ROOT}/usr/bin")

    # lazy hack to fix include path issue with the pip install of sip
    vcpkg_host_path_list(PREPEND ENV{INCLUDE} "${_VCPKG_INSTALLED_DIR}/${TARGET_TRIPLET}/include/python3.9/")
    vcpkg_host_path_list(PREPEND ENV{INCLUDE} "${_VCPKG_INSTALLED_DIR}/${TARGET_TRIPLET}/include/python3.11/")
    vcpkg_host_path_list(PREPEND ENV{LIB} "${_VCPKG_INSTALLED_DIR}/${TARGET_TRIPLET}/lib/")
endif()


vcpkg_execute_build_process(
    COMMAND ${PYTHON3} ./build.py dox
    WORKING_DIRECTORY "${SOURCE_PATH}"
    LOGNAME "prepare-dox-${RELEASE_TRIPLET}"
)

vcpkg_execute_build_process(
    COMMAND ${PYTHON3} ./build.py etg --nodoc sip
    WORKING_DIRECTORY "${SOURCE_PATH}"
    LOGNAME "prepare-etg-${RELEASE_TRIPLET}"
)

z_vcpkg_apply_patches(
    SOURCE_PATH "${SOURCE_PATH}"
    PATCHES
        patch-sip-gc.patch
)

vcpkg_cmake_configure(
    SOURCE_PATH ${SOURCE_PATH}
)

vcpkg_cmake_install()

if(CMAKE_HOST_WIN32)
    unset(Python3_EXECUTABLE CACHE)
    unset(PYTHON3)
    unset(ENV{VIRTUAL_ENV})
    unset(ENV{PYTHONPATH})
    unset(ENV{PYTHONPATH})
    
    # this is a hack to make things work (i.e. just running from build dir and packaging)
	set(Python3_FIND_REGISTRY "NEVER")
    set(PYTHON3_ROOT_DIR "${_VCPKG_INSTALLED_DIR}/${HOST_TRIPLET}/tools/python3")
    vcpkg_add_to_path(PREPEND "${PYTHON3_ROOT_DIR}")

    set(ENV{PYTHONHOME} "${PYTHON3_ROOT_DIR}")
    set(ENV{PYTHONPATH} "${PYTHON3_ROOT_DIR}/DLLs;${PYTHON3_ROOT_DIR}/Lib")
    
    find_package( Python3 COMPONENTS Interpreter REQUIRED )

    vcpkg_execute_build_process(
        COMMAND ${Python3_EXECUTABLE} -m ensurepip
        WORKING_DIRECTORY "${SOURCE_PATH}"
        LOGNAME "prepare-ensurepip-${RELEASE_TRIPLET}"
    )

    vcpkg_execute_build_process(
        COMMAND ${Python3_EXECUTABLE} -m pip install -t ${CURRENT_PACKAGES_DIR}/tools/python3/Lib/site-packages/ -r requirements/install.txt
        WORKING_DIRECTORY "${SOURCE_PATH}"
        LOGNAME "prepare-requirements-${RELEASE_TRIPLET}"
    )
endif()

file(INSTALL ${SOURCE_PATH}/wx/ DESTINATION ${CURRENT_PACKAGES_DIR}/tools/python3/Lib/site-packages/wx/)
file(INSTALL ${SOURCE_PATH}/wx/include/wxPython/ DESTINATION ${CURRENT_PACKAGES_DIR}/include/wxPython/)

# Per https://www.wxpython.org/pages/license/, using wxWindows Library License, no license in the source repo
file(INSTALL ${CMAKE_CURRENT_LIST_DIR}/copyright DESTINATION ${CURRENT_PACKAGES_DIR}/share/${PORT})